Bootstrap: docker
From: ubuntu

%setup
     ## The "%setup"-part of this script is called to bootstrap an empty
     ## container. It copies the source files from the branch of your
     ## repository where this file is located into the container to the
     ## directory "/planner". Do not change this part unless you know
     ## what you are doing and you are certain that you have to do so.

    REPO_ROOT=`dirname $SINGULARITY_BUILDDEF`
    cp -r $REPO_ROOT/ $SINGULARITY_ROOTFS/planner

%post

    ## The "%post"-part of this script is called after the container has
    ## been created with the "%setup"-part above and runs "inside the
    ## container". Most importantly, it is used to install dependencies
    ## and build the planner. Add all commands that have to be executed
    ## once before the planner runs in this part of the script.

    ## Install all necessary dependencies.
    apt-get update
    apt-get -y install cmake g++ make python python-pip automake autoconf libtool time gawk flex bison zip unzip
    pip install networkx

    ## Build your planner
    cd /planner
    ./build -j4

%runscript
    ## The runscript is called whenever the container is used to solve
    ## an instance.

    DOMAINFILE=$1
    PROBLEMFILE=$2
    PLANFILE=$3

    ## Call your planner.
    /planner/plan $DOMAINFILE $PROBLEMFILE $PLANFILE

## Update the following fields with meta data about your submission.
## Please use the same field names and use only one line for each value.
%labels
Name        Symple-2 
Description Symple performs a bidirectional A* search based on Edge-Valued Multi-Valued Decision Diagrams (EVMDDs). Symple-2 differs from Symple-1 only in the translation step. Symple-2 uses a symmetry reduction procedure to detect operations that are duplicates.  For example, in the researchers domain, the researchers are ordered in such a way that only one action is created instead of several identical ones for the publish-5 action. 
Authors     David Speck, Florian Geißer and Robert Mattmüller <speckd,geisserf,mattmuel>@informatik.uni-freiburg.de 
SupportsDerivedPredicates no
SupportsQuantifiedPreconditions no
SupportsQuantifiedEffects no
